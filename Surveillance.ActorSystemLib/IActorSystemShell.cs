﻿namespace Surveillance.ActorSystemLib
{
    public interface IActorSystemShell
    {
        void Start();
        void Stop();
    }
}
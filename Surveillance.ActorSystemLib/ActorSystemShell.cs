﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Monitoring;
using Akka.Monitoring.ZeroMQ;
using Surveillance.ActorSystemLib.Actors;

namespace Surveillance.ActorSystemLib
{
    public class ActorSystemShell:IActorSystemShell
    {
        private ActorSystem _system;
        private bool _registeredMonitor;

        public void Start()
        {
            _system = ActorSystem.Create("simulation");
            _registeredMonitor = ActorMonitoringExtension.RegisterMonitor(_system, new ZeroMqClientMonitoringActor());
            _system.ActorOf<ClientActor>();
        }

        public void Stop()
        {
            _system.Terminate();
        }
    }
}

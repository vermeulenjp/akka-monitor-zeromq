﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Surveillance.ActorSystemLib.Messages
{
    public class DepositMsg
    {
        public DepositMsg(int amount)
        {
            Amount = amount;
        }

        public int Amount { get; }
    }
}

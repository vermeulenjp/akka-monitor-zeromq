﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Surveillance.ActorSystemLib.Messages
{
    public class BalanceMsg
    {
        public BalanceMsg(int balance)
        {
            Balance = balance;
        }

        public int Balance { get; }
    }
}

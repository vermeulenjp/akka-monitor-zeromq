﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Surveillance.ActorSystemLib;

namespace Surveillaince.App.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var system = new ActorSystemShell();
            system.Start();
            Console.ReadKey();
        }
    }
}

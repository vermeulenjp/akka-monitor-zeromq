﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Monitoring.Impl;
using ZeroMQ;

namespace Akka.Monitoring.ZeroMQ
{
    public class ZeroMqClientMonitoringActor:AbstractActorMonitoringClient
    {
        private readonly ZContext _zeroContext;
        private readonly ZSocket _zeroRequest;
        private readonly string _endPoint;

        public ZeroMqClientMonitoringActor()
        {
            _zeroContext = new ZContext();
            _zeroRequest = new ZSocket(_zeroContext,ZSocketType.PUSH);
            _endPoint = "tcp://127.0.0.1:5555";
            _zeroRequest.Connect(_endPoint);
            MonitoringClientId = 1;
        }

        public override void UpdateCounter(string metricName, int delta, double sampleRate)
        {
            _zeroRequest.Send(new ZFrame($"{metricName}|{delta}|{sampleRate}"));
        }

        public override void UpdateTiming(string metricName, long time, double sampleRate)
        {
            _zeroRequest.Send(new ZFrame($"{metricName}|{time}|{sampleRate}"));
        }

        public override void UpdateGauge(string metricName, int value, double sampleRate)
        {
            _zeroRequest.Send(new ZFrame($"{metricName}|{value}|{sampleRate}"));
        }

        public override int MonitoringClientId { get; }

        public override void DisposeInternal()
        {
            _zeroRequest.Disconnect(_endPoint);
            _zeroRequest.Dispose();
            _zeroContext.Dispose();
        }
    }
}
